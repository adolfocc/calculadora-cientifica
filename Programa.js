class Calculadora{

    numero1
    numero2
    sum
    res 
    div
    mul
    mod

    sumar(){
        this.sum= this.numero1 + this.numero2 
    }
    restar(){
        this.res= this.numero1 - this.numero2
    }
    dividir(){
        this.div= this.numero1 / this.numero2
    }
    multiplicar(){
        this.mul= this.numero1 * this.numero2
    }
    modulo(){
        this.mod= this.numero1 % this.numero2
    }

    get gSum(){
        return this.sum
    }
    get gRes(){
        return this.res
    }
    get gDiv(){
        return this.div
    }
    get gMul(){
        return this.mul
    }
    get gMod(){
        return this.mod
    }

    constructor(a,b){
        this.numero1=a
        this.numero2=b
    }
}

class CalculadoraCientifica extends Calculadora{
    numero3
    sen
    cos
    tan
    seno(){
        this.sen= Math.sin(Math.PI*this.numero3)
    }

    coseno(){
        this.cos= Math.cos(Math.PI*this.numero3)
    }

    tangente(){
        this.tan= Math.tan(Math.PI*this.numero3)
    }

    get gSen(){
        return this.sen
    }
    get gCos(){
        return this.cos
    }
    get gTan(){
        return this.tan
    }


    constructor(a,b,c){
        super(a,b)
        this.numero3=c
    }
}

class CalculadoraConversora extends CalculadoraCientifica{
    numero4
    numero5
    numero6
    numero7
    gAr
    rAg
    kAc
    cAk


    gradosARadianes(){
        this.gAr= (this.numero4*(Math.PI/180))
    }
    radianesAGrados(){
        this.rAg= (this.numero5*180)
    }
    kelvinACentigrados(){
        this.kAc= this.numero6 - 273.15
    }
    centigradosAKelvin(){
        this.cAk= this.numero7 + 273.15
    }

    get gAr(){
        return this.gAr
    }
    get rAg(){
        return this.rAg
    }
    get kAc(){
        return this.kAc
    }
    get cAk(){
        return this.cAk
    }

    constructor(a,b,c,d,e,f,g){
        super(a,b,c)
        this.numero4=d
        this.numero5=e
        this.numero6=f
        this.numero7=g
    }
}

class todas extends CalculadoraConversora{
    calc(){
        this.sumar()
        this.restar()
        this.dividir()
        this.multiplicar()
        this.modulo()
        this.seno()
        this.coseno()
        this.tangente()
        this.gradosARadianes()
        this.radianesAGrados()
        this.kelvinACentigrados()
        this.centigradosAKelvin()
    }
    constructor(a,b,c,d,e,f,g){
        super(a,b,c,d,e,f,g)
    }
}